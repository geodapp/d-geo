package go_services

import (
	log "logutil"

	"github.com/go-ini/ini"
)

var settings *ini.File

func InitializeServices() {
	log.InitializeLogger()

	settings, err := ini.Load("settings.ini")
	if err != nil {
		log.Info("Error while parsing settings.")
	}

	log.Info("GO-Services have been initialized!")
}
