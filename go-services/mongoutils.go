/*
	File Name: mongoutils.go
	Author: Elio Decolli (eliodecolli@gmail.com)
	Purpose: Functions used to interact with MongoDB.
*/

package go_services

import (
	"context"
	log "logutil"

	"github.com/mongodb/mongo-go-driver/x/bsonx"

	"time"

	"fmt"
	"math/rand"

	"github.com/mongodb/mongo-go-driver/bson"
	mongo "github.com/mongodb/mongo-go-driver/mongo"
)

const (
	PurchaseRequest = iota
	NewDevice       = iota
)

type User struct {
	id       int64  // Unique user ID
	name     string // Username
	email    string // User email address
	password string // this is a hash btw :)

	isOnline      bool  // Current presence status
	lastHeartBeat int64 // Needed to check for auto-log-out
}

type UserDevice struct {
	uId int64 // Owner ID
	dId int   // Device ID

	name string // Device Name
}

type UserTransaction struct {
	uId  int64 // owner of the transaction
	txId int   // transaction id

	buyerName string // a name for the buyer might be needed ?
	buyerAddr string // public Ethereum address of the buyer
	date      int64  // Unix-format time-date for the transaction, when it occured

	txAddr string // public Ethereum address of the transaction
}

type UserRequest struct {
	uId int64 // Target user id for this request
	rId int   // Unique request ID for a specific user

	reqType int // Type of the request

	reqData []byte // Data
	reqLen  int    // Data Length

	reqTime int64 // Time of the request
}

func GetUserById(ctx context.Context, id int64, client *mongo.Client) *User {
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return nil
	}

	collection := client.Database("users").Collection("user_data")
	retval := User{}

	collection.FindOne(ctx, bson.M{"id": id}).Decode(&retval)
	return &retval
}

func IsUserPresent(ctx context.Context, client *mongo.Client, email string) bool {
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return false
	}

	collection := client.Database("users").Collection("user_data")
	temp := User{}
	res := collection.FindOne(ctx, bson.M{"email": email}).Decode(&temp)
	return (res == nil)
}

func IsUserPresentId(ctx context.Context, client *mongo.Client, uid int64) bool {
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return false
	}

	collection := client.Database("users").Collection("user_data")
	temp := User{}
	res := collection.FindOne(ctx, bson.M{"id": uid}).Decode(&temp)
	return (res == nil)
}

func TryLogin(ctx context.Context, email string, password string, client *mongo.Client) (*User, bool) {
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return nil, false
	}

	collection := client.Database("users").Collection("user_data")
	retval := User{}

	pp := collection.FindOne(ctx, bson.M{"email": email, "password": password}).Decode(&retval)
	if pp != nil {
		return nil, false
	}
	retval.lastHeartBeat = time.Now().Unix()
	up, err := collection.UpdateOne(ctx, bson.M{"email": email, "password": password}, retval)
	if err != nil {
		return nil, false
	}

	if up.ModifiedCount == 0 {
		log.Info(fmt.Sprintf("Unable to set lastHeartBeat for user with ID: %d", retval.id))
	}

	return &retval, true
}

func HeartBeat(ctx context.Context, id int64, client *mongo.Client) bool {
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return false
	}

	collection := client.Database("users").Collection("user_data")

	retval := GetUserById(ctx, id, client)
	if retval == nil {
		log.Info(fmt.Sprintf("Unable to find user with ID %d", id))
		return false
	}

	retval.lastHeartBeat = time.Now().Unix()
	up, err := collection.UpdateOne(ctx, bson.M{"id": id}, retval)

	if err != nil {
		log.Info(fmt.Sprintf("Unable to set lastHeartBeat for user with ID %d (Error: %s)", id, err.Error()))
		return false
	}

	if up.ModifiedCount == 0 {
		log.Info(fmt.Sprintf("Unable to set lastHeartBeat for user with ID %d", id))
		return false
	}

	return true
}

func TryRegisterUserRequest(ctx context.Context, client *mongo.Client, id int64, rType int, rData []byte, rLen int) (int, bool) {
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return 0, false
	}

	collection := client.Database("users").Collection("user_notifications")
	temp := UserRequest{uId: id, rId: rand.Int(), reqType: rType, reqData: rData, reqLen: rLen, reqTime: time.Now().Unix()}
	_, pp := collection.InsertOne(ctx, bson.D{{"uId", bsonx.Int64(id)},
		{"rId", bsonx.Int32(int32(temp.rId))},
		{"reqType", bsonx.Int32(int32(rType))},
		{"reqData", bsonx.Binary(0x00, rData)},
		{"reqLen", bsonx.Int32(int32(rLen))},
		{"reqTime", bsonx.Int64(temp.reqTime)}})
	if pp != nil {
		log.Info(fmt.Sprintf("Unexpected error occured (ERROR: %s)", pp.Error()))
		return 0, false
	}
	return temp.rId, true
}

func TryRegisterTransaction(ctx context.Context, client *mongo.Client, id int64, mTxAddr string, bName string, bAddr string, txDate int64) (int, bool) {
	log.Info(fmt.Sprintf("Registering user tx for UID %d", id))
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return 0, false
	}

	collection := client.Database("users").Collection("user_tx")
	temp := UserTransaction{uId: id, txId: rand.Int(), buyerName: bName, buyerAddr: bAddr, date: txDate, txAddr: mTxAddr}
	_, pp := collection.InsertOne(ctx, bson.D{{"uId", bsonx.Int64(id)},
		{"txId", bsonx.Int32(int32(temp.txId))},
		{"buyerName", bsonx.String(bName)},
		{"buyerAddr", bAddr},
		{"date", bsonx.Int64(temp.date)},
		{"txAddr", bsonx.String(temp.txAddr)}})
	if pp != nil {
		log.Info(fmt.Sprintf("Unexpected error while registering transaction (ERROR: %s)", pp.Error()))
		return 0, false
	}

	return temp.txId, true
}

func TryRegisterDeviceForUser(ctx context.Context, client *mongo.Client, id int64, dName string) (int, bool) {
	log.Info(fmt.Sprintf("Registering new Device '%s' for user with ID %d", dName, id))

	if !IsUserPresentId(ctx, client, id) {
		log.Info("User ID is invalid.")
		return 0, false
	}
	err := client.Ping(ctx, nil)
	if err != nil {
		log.Info("Unable to connect to MongoDB instance.")
		return 0, false
	}
	temp := UserDevice{uId: id, dId: rand.Int(), name: dName}
	collection := client.Database("users").Collection("user_devices")
	_, pp := collection.InsertOne(ctx, bson.D{{"uId", bsonx.Int64(id)},
		{"dId", bsonx.Int32(int32(temp.dId))},
		{"name", bsonx.String(temp.name)}})
	if pp != nil {
		log.Info(fmt.Sprintf("Unexpected error while registering device (ERROR: %s)", err.Error()))
		return 0, false
	}
	return temp.dId, true
}

/*
  Returns 4 if success, others if something unexpected happened.
*/
func TryRegisterUser(ctx context.Context, client *mongo.Client, uemail string, upassword string, username string) (int64, bool, int) {
	log.Info(fmt.Sprintf("Trying to register user with email %s", uemail))
	autoReg, err := settings.Section("server").Key("auto_registration").Bool()
	if err != nil {
		log.Info("Error while parsing 'auto_reg' value from settings.")
		return 0, false, 0
	}
	if autoReg == false {
		log.Info("Server has been configured not to allow automatic registrations.")
		return 0, false, 1
	} else {
		if !IsUserPresent(ctx, client, uemail) {
			temp := User{name: username, email: uemail, password: upassword, id: rand.Int63(), isOnline: false, lastHeartBeat: 0}
			collection := client.Database("users").Collection("user_data")
			_, err := collection.InsertOne(ctx, bson.D{{"id", bsonx.Int64(temp.id)},
				{"name", bsonx.String(temp.name)},
				{"email", bsonx.String(temp.email)},
				{"password", bsonx.String(temp.password)},
				{"isOnline", bsonx.Boolean(temp.isOnline)},
				{"lastHeartBeat", bsonx.Int64(temp.lastHeartBeat)}})
			if err != nil {
				log.Info("Unexpected error while registering user.")
				return 0, false, 3
			}
			return temp.id, true, 4
		} else {
			log.Info("Email address is already taken.")
			return 0, false, 2
		}
	}
}
